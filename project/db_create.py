# project/db_create.py
# Creates and initializes the FlaskTaskr database
from views import db
from models import Task
from datetime import date


# Create the database and the db table
db.create_all()

# Insert Data
db.session.add(Task("Finish this tutorial", date(2019, 1, 20), 10, 1))
db.session.add(Task("Finish RealPython", date(2019, 4, 1), 5, 1))
db.session.add(Task("Quit ClearCaptions", date(2019, 1, 22), 10, 1))
db.session.add(Task("Start Peloton", date(2019, 1, 29), 10, 1))

# Commit the changes
db.session.commit()
